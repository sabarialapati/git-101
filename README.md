# Welcome to GIT 101!!!!!!

We are so thrilled you are here!

# Pre-Reqs

## Bitbucket Account

# hello from robert 

Do you know your Bitbucket account information?
You should have received an invite to this repo prior to the meeting.

## SSH Keys

The more traditional route and the one that we will use most often, is by configuring SSH Keys to interact with Git repositories. This is what you will use when contirbuting to most Git platforms such as Github anf Gitlab.

1. Open up your [Profile Settings](https://bitbucket.org/account/settings/) in Bitbucket.
   ![](./img/2022-02-07-13-01-42.png)
2. Select `SSH Keys`
   ![](./img/2022-02-07-13-02-09.png)
3. Open up `PowerShell` and run the command to generate a new SSH Key Pair.
   ```powershell
   $ ssh-keygen
    Generating public/private rsa key pair.
    Enter file in which to save the key (/c/Users/emmap1/.ssh/id_rsa):
    Created directory '/c/Users/emmap1/.ssh'.
    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:
    Your identification has been saved in /c/Users/emmap1/.ssh/id_rsa.
    Your public key has been saved in /c/Users/emmap1/.ssh/id_rsa.pub.
    The key fingerprint is: e7:94:d1:a3:02:ee:38:6e:a4:5e:26:a3:a9:f4:95:d4 emmap1@EMMA-PC
   ```
4. Copy the content of your new `Public Key`
   ```powershell
   get-content C:\users\kyle.parrish\.ssh\id_rsa.pub | clip
   ```
5. Click `Add Key`
   ![](./img/2022-02-07-13-05-20.png)
6. Paste in the key and give it a name (any name will do)
   ![](./img/2022-02-07-13-05-46.png)

## VSCode

VSCode has a handy plugin to help connect Bitbucket with VSCode. If you want to test it out, here are the steps to configure it.

1. Open up the "Extensions" tab on the left and search for `Bitbucket`.
   ![](./img/2022-02-07-12-58-12.png)
2. Select `Bitbucket`
   ![](./img/2022-02-07-12-53-11.png)
3. We want to use `Bitbucket Cloud`.
   ![](./img/2022-02-07-12-53-40.png)
4. Confirm access.
   ![](./img/2022-02-07-12-54-00.png)
5. Go back to VSCode.
   ![](./img/2022-02-07-12-54-16.png)
6. Allow VSCode to open the URL.
   ![](./img/2022-02-07-12-54-34.png)
7. We are ready to get started!
   ![](./img/2022-02-07-12-54-47.png)
